<?php


class BookDao
{
    private $connection;
    public function __construct(){
        $this ->connection = $this ->connectToDb();
    }
    private function connectToDb (){
        $user = 'taurituuling';
        $pass = 'd40c';
        $address = 'mysql:host=db.mkalmo.xyz;dbname=taurituuling';
        return $connection = new PDO($address, $user, $pass,[PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]);
    }
    public function getallBooks() {
        $this->connectToDb();
        $request = "SELECT books.id, books.title, books.grade, books.authorid, books.isread, authors.firstname, authors.lastname FROM books LEFT JOIN authors ON authorid=authors.id";
        $stmt = $this ->connection->prepare($request);
        $stmt -> execute();
        $books = [];
        foreach ($stmt as $row){
            $book = new book($row['id'], $row['title'], $row['isread'], $row['grade'], $row['authorid'], $row['firstname'] . " " . $row['lastname']);
            $books[] = $book;
        }
        return $books;
    }
    public function getBookById($id){
        $this->connectToDb();
        $request = "SELECT books.id, books.title, books.isread, books.grade, books.authorid FROM books WHERE id=$id";
        $stmt = $this ->connection->prepare($request);
        $stmt -> execute();
        foreach ($stmt as $row) {
            $book = new book($row['id'], $row['title'], $row['isread'], $row['grade'], $row['authorid'], '');
        }
        return $book;
    }
}