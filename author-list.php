<!DOCTYPE html>
<html lang="et">
<head>
    <meta charset="utf-8">
    <title>Harjutustund 1</title>
    <link href="styles.css" rel="stylesheet">
</head>
<body>
<?php
$file = fopen("authors.txt", "r");
function readL($file)
{
    $arr = [];
    $x = 0;
    while (!feof($file)) {

        $arr[$x] = explode(":", fgets($file));
        if (isset($arr[$x][0])) {
            $arr[$x][0] = urldecode($arr[$x][0]);
        }
        if (isset($arr[$x][1])) {
            $arr[$x][1] = urldecode($arr[$x][1]);
        }
        $x += 1;
    }
    array_pop($arr);
    return $arr;
}
$books = readL($file);
fclose($file);
const USERNAME = 'taurituuling';
const PASSWORD = 'd40c';
const ADDRESS = 'mysql:host=db.mkalmo.xyz;dbname=taurituuling';

function connectToDb($user,$pass,$address){
    return $connection = new PDO($address, $user, $pass,[PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]);
}
function getFromDb(){
    $connection = connectToDb(USERNAME, PASSWORD, ADDRESS);
    $request = "SELECT id, firstname, lastname, grade FROM authors";
    $stmt = $connection ->prepare($request);
    $stmt -> execute();
    $result = $stmt->setFetchMode(PDO::FETCH_ASSOC);
    return $stmt->fetchAll();
}


function printTable($data){
    foreach ($data as $x => $value ){
        echo "<tr><td>" . "<a href='author-edit.php?id=" . $data[$x]['id'] . "'>" . $data[$x]['firstname'] . "</a>" . "</td>" . "<td>" . $data[$x]['lastname'] . "</td>" . "<td>" . $data[$x]['grade'] . "</td>" . "</tr>";
    }
}
?>
<header>
    <a href="?cmd=show_book_list" id="book-list-link">Raamatud |</a>
    <a href="?cmd=show_book_form" id="book-form-link">Lisa raamat |</a>
    <a href="?cmd=show_author_list" id="author-list-link">Autorid |</a>
    <a href="?cmd=show_author_form" id="author-form-link">Lisa autor</a>
    <br/>
</header>
<?php
if (isset($_GET["message"])) {
    $message = $_GET["message"];
    if ($message == "success") {
        echo "<p id='message-block'>success</p>";
    }
}
?>
    <table class="table_head">
        <tr >
            <th class="table_col1"><strong>Eesnimi</strong></th>
            <th class="table_col2"><strong>Perekonnanimi</strong></th>
            <th><strong>Hinne</strong></th>
        </tr>
    </table>
    <table class="tabel1">
        <tr>
            <td class="table_col1">Eesnimi 1</td>
            <td class="table_col2">Perekonnanimi 1</td>
            <td>Hinne 1</td>
        </tr>
        <tr>
            <td class="table_col1">Eesnimi 2</td>
            <td class="table_col2" >Perekonnanimi 2</td>
            <td>Hinne 2</td>
        </tr>
        <?php printTable(getFromDb());?>
    </table>

    <footer>
ICD0007 Näidisrakendus
</footer>

</body>
</html>