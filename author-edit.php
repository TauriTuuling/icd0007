<!DOCTYPE html>
<html lang="et">
<head>
    <meta charset="utf-8">
    <title>Harjutustund 1</title>
    <link href="styles.css" rel="stylesheet">
</head>
<body>
<?php
$id = $_GET['id'];
if(intval($id) == 0){
    $id = 1;
}

function gradeCheck($grade, $field){
    if ($field == $grade) {
        echo "checked";
    }
}
const USERNAME = 'taurituuling';
const PASSWORD = 'd40c';
const ADDRESS = 'mysql:host=db.mkalmo.xyz;dbname=taurituuling';
function connectToDb($user,$pass,$address){
    return $connection = new PDO($address, $user, $pass,[PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]);
}

function getFromDb($id){
    $connection = connectToDb(USERNAME, PASSWORD, ADDRESS);
    $request = "SELECT id, firstname, lastname, grade FROM authors WHERE id=$id";
    $stmt = $connection ->prepare($request);
    $stmt -> execute();
    $result = $stmt->setFetchMode(PDO::FETCH_ASSOC);
    return $stmt->fetchAll();
}
$authorData = getFromDb($id);
?>

<header>
    <a href="book-list.php" id="book-list-link">Raamatud |</a>
    <a href="book-add.php" id="book-form-link">Lisa raamat |</a>
    <a href="author-list.php" id="author-list-link">Autorid |</a>
    <a href="author-add.php" id="author-form-link">Lisa autor</a>
    <br/>
</header>

<form action="edit-delete-author.php" name="myForm">
    <table class="input-table">

        <tr>
            <td><label for="eesn" >Eesnimi: </label></td>
            <td><input type="text" id="eesn" name="firstName" value="<?php echo $authorData[0]['firstname']?>"></td>
        </tr>
        <tr>
            <td><label for="peren" >Perekonnanimi: </label></td>
            <td><input type="text" id="peren" name="lastName" value="<?php echo $authorData[0]['lastname']?>"></td>
        </tr>
        <tr>
            <td><label for="grade">Hinne:</label></td>
            <td>
                <input type="radio" name="grade" id="grade" value="1" <?php gradeCheck($authorData[0]['grade'], 1); ?>>1
                <label for="2"></label><input type="radio" name="grade" value="2" id="2" <?php gradeCheck($authorData[0]['grade'], 2); ?>>2
                <label for="3"></label><input type="radio" name="grade" value="3" id="3" <?php gradeCheck($authorData[0]['grade'], 3); ?>>3
                <label for="4"></label><input type="radio" name="grade" value="4" id="4" <?php gradeCheck($authorData[0]['grade'], 4); ?>>4
                <label for="5"></label><input type="radio" name="grade" value="5" id="5" <?php gradeCheck($authorData[0]['grade'], 5); ?>>5</td>
        </tr>
        <tr>
            <input type='hidden' name='id' value='<?php echo "$id";?>'/>
            <td><input type="submit" value="Kustuta" name="deleteButton"></td>
            <td><input type="submit" value="Salvesta" name="submitButton"></td>
        </tr>

    </table>
</form>
<footer>
    ICD0007 Näidisrakendus
</footer>
</body>
</html>