<?php


class author
{
    public $id;
    public $firstname;
    public $lastname;
    public $grade;
    public function __construct($id, $firstname, $lastname, $grade) {
        $this->id=intval($id);
        $this->firstname=$firstname;
        $this->lastname=$lastname;
        $this->grade=$grade;
    }

}