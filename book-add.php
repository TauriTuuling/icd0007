<!DOCTYPE html>
<html lang="et">
<head>
    <meta charset="utf-8">
    <title>Harjutustund 1</title>
    <link href="styles.css" rel="stylesheet">
</head>
<body>
<header id="header">
    <a href="?cmd=show_book_list" id="book-list-link">Raamatud |</a>
    <a href="?cmd=show_book_form" id="book-form-link">Lisa raamat |</a>
    <a href="?cmd=show_author_list" id="author-list-link">Autorid |</a>
    <a href="?cmd=show_author_form" id="author-form-link">Lisa autor</a>
    <br/>
</header>
<?php
    if (isset($_GET["error"])){
        $error = $_GET["error"];
        $title = $_GET["title"];
        $grade = $_GET["grade"];
        $read = $_GET["isRead"];
        if ($read == 1){
            $read = "checked";
        }
        echo "<p id='error-block'>ERROR</p>";
    }
    else{$grade=0;$read="off"; $title="";}

const USERNAME = 'taurituuling';
const PASSWORD = 'd40c';
const ADDRESS = 'mysql:host=db.mkalmo.xyz;dbname=taurituuling';

function connectToDb($user,$pass,$address){
    return $connection = new PDO($address, $user, $pass,[PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]);
}
function getFromDb(){
    $connection = connectToDb(USERNAME, PASSWORD, ADDRESS);
    $request = "SELECT id, firstname, lastname, grade FROM authors";
    $stmt = $connection ->prepare($request);
    $stmt -> execute();
    $result = $stmt->setFetchMode(PDO::FETCH_ASSOC);
    return $stmt->fetchAll();
}
    function gradeCheck($grade, $field){
        if ($field == $grade) {
            echo "checked";
        }
    }
    function showAuthors($data){
        foreach ($data as $x => $value ){
            echo "<option value='" . $data[$x]['id'] . "'>".$data[$x]['firstname'] . " " . $data[$x]['lastname'] ."</option>";
        }
    }
?>
<form action="book-add-to-file.php" name="myForm">
    <table class="input-table">
        <tr>
            <td><label for="plkr" >Pealkiri: </label></td>
            <td><input type="text" id="plkr" name="title" value="<?php echo $title;?>"></td>
        </tr>
        <tr>
            <td><label for="author1">Autor 1:</label></td>
            <td><select id="author1" name="author1">
                    <option value="0"></option>
                    <?php showAuthors(getFromDb()); ?>
                </select>
            </td>
        </tr>

        <tr>
            <td><label for="author2">Autor 2:</label></td>
            <td><select id="author2">
                    <option></option>
                    <option>AA</option>
                </select></td>
        </tr>
        <tr>
            <td><label for="grade">Hinne:</label></td>
            <td>
                <input type="radio" name="grade" id="grade" value="1" <?php gradeCheck($grade, 1); ?> >1
                <label>
                    <input type="radio" name="grade" value="2" <?php gradeCheck($grade, 2); ?>>
                </label>2
                <label>
                    <input type="radio" name="grade" value="3" <?php gradeCheck($grade, 3); ?>>
                </label>3
                <label>
                    <input type="radio" name="grade" value="4" <?php gradeCheck($grade, 4); ?>>
                </label>4
                <label>
                    <input type="radio" name="grade" value="5" <?php gradeCheck($grade, 5); ?>>
                </label>5</td>
        </tr>

        <tr>
            <td><label for="isRead">Loetud</label></td>
            <td><input type="checkbox" id="isRead" name="isRead" <?php echo $read;?>><br></td>
        </tr>
        <tr>
            <td></td>
            <td><input type="submit" value="Salvesta" name="submitButton"></td>
        </tr>
    </table>
</form>
<footer>
    ICD0007 Näidisrakendus
</footer>
</body>
</html>