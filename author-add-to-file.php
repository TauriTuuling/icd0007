<?php
$inputFirstName = $_GET["firstName"];
$inputLastName = $_GET["lastName"];
if (isset($_GET["grade"])){
    $inputGrade = $_GET["grade"];
}
else{
    $inputGrade = 0;
}



const USERNAME = 'taurituuling';
const PASSWORD = 'd40c';
const ADDRESS = 'mysql:host=db.mkalmo.xyz;dbname=taurituuling';

function connectToDb($user,$pass,$address){
    return $connection = new PDO($address, $user, $pass,[PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]);
}

if ($inputGrade == "5"){
    $inputGrade = 5;
}
else if($inputGrade == "4"){
    $inputGrade = 4;
}
else if($inputGrade == "3"){
    $inputGrade = 3;
}
else if($inputGrade == "") {
    $inputGrade = 0;
}

function addAuthor($fname, $lname, $grade){
    $connection = connectToDb(USERNAME, PASSWORD, ADDRESS);
    $data = "INSERT INTO authors (firstname, lastname, grade) VALUES(:firstname,:lastname,:grade)";
    $stmt = $connection ->prepare($data);
    $stmt -> bindValue(":firstname", $fname);
    $stmt -> bindValue(":lastname", $lname);
    $stmt -> bindValue(":grade", $grade);
    $stmt ->execute();
    unset($stmt);

}
if (strlen($inputFirstName) > 0 and strlen($inputFirstName) < 22 and
    strlen($inputLastName) > 1 and strlen($inputLastName) < 23)
{
    $authors = "authors.txt";
    $currentFile = file_get_contents($authors);
    $currentFile .= urlencode($inputFirstName) . ":" . urlencode($inputLastName) . ":" . $inputGrade . "\n";
    file_put_contents($authors, $currentFile);
    addAuthor($inputFirstName, $inputLastName, $inputGrade);
    header("Location: author-list.php?message=success");
}
else {
    header("Location: author-add.php?" . "firstname=" . $inputFirstName . "&error=true" . "&grade=" . $inputGrade .
        "&lastname=" . $inputLastName);
}
