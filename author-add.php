<!DOCTYPE html>
<html lang="et">
<head>
    <meta charset="utf-8">
    <title>Harjutustund 1</title>
    <link href="styles.css" rel="stylesheet">
</head>
<body>
<header>
    <a href="?cmd=show_book_list" id="book-list-link">Raamatud |</a>
    <a href="?cmd=show_book_form" id="book-form-link">Lisa raamat |</a>
    <a href="?cmd=show_author_list" id="author-list-link">Autorid |</a>
    <a href="?cmd=show_author_form" id="author-form-link">Lisa autor</a>
    <br/>
</header>
<?php
if (isset($_GET["error"])) {
    $error = $_GET["error"];
    $firstname = $_GET["firstname"];
    $grade = $_GET["grade"];
    $lastname = $_GET["lastname"];
    if ($error == "true") {
        echo "<p id='error-block'>ERROR</p>";
    }
}
else{$grade=0;$read="off"; $firstname="";$lastname="";}
function gradeCheck($grade, $field){
    if ($field == $grade) {
        echo "checked";
    }
}
?>
<form action="author-add-to-file.php">
    <table class="input-table">

        <tr>
            <td><label for="eesn">Eesnimi: </label></td>
            <td><input type="text" id="eesn" name="firstName" value="<?php echo $firstname ?>"><br></td>
        </tr>

        <tr>

            <td><label for="perekn">Perekonnanimi: </label></td>
            <td><input type="text" id="perekn" name="lastName" value="<?php echo $lastname ?>"><br></td>
        </tr>

        <tr>
            <td><label for="grade">Hinne:</label></td>
            <td><input type="radio" name="grade" id="grade" value="1" <?php gradeCheck($grade, 1); ?>>1
                <label>
                    <input type="radio" name="grade" value="2" <?php gradeCheck($grade, 2); ?>>
                </label>2
                <label>
                    <input type="radio" name="grade" value="3" <?php gradeCheck($grade, 3); ?>>
                </label>3
                <label>
                    <input type="radio" name="grade" value="4" <?php gradeCheck($grade, 4); ?>>
                </label>4
                <label>
                    <input type="radio" name="grade" value="5" <?php gradeCheck($grade, 5); ?>>
                </label>5<br></td>
        </tr>

        <tr><td></td>
            <td><input type="submit" value="Salvesta" name="submitButton"></td></tr>

    </table>
</form>
<footer>
    ICD0007 Näidisrakendus
</footer>
</body>
</html>