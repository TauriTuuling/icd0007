<!DOCTYPE html>
<html lang="et">
<head>
    <meta charset="utf-8">
    <title>Harjutustund 1</title>
    <link href="styles.css" rel="stylesheet">
    <script>
        function validateForm() {
            let x = document.forms["myForm"]["title"].value;
            if (x.length < 3 || x.length > 23) {
                alert("Pealkiri peab olema 3 kuni 23 märki pikk!");
                return false;
            }
        }

    </script>
</head>
<body>
<?php
$id = $_GET['id'];
if(intval($id) == 0){
    $id = 1;
}

function gradeCheck($grade, $field){
    if ($field == $grade) {
        echo "checked";
    }
}
const USERNAME = 'taurituuling';
const PASSWORD = 'd40c';
const ADDRESS = 'mysql:host=db.mkalmo.xyz;dbname=taurituuling';
function connectToDb($user,$pass,$address){
    return $connection = new PDO($address, $user, $pass,[PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]);
}

function getFromDb($id){
    $connection = connectToDb(USERNAME, PASSWORD, ADDRESS);
    $request = "SELECT id, title, isread, grade, authorid FROM books WHERE id=$id";
    $stmt = $connection ->prepare($request);
    $stmt -> execute();
    $result = $stmt->setFetchMode(PDO::FETCH_ASSOC);
    return $stmt->fetchAll();
}

$bookData = getFromDb($id);
if ($bookData[0]['isread'] == 1){
    $read = "checked";
}
else{
    $read = "";
}
function getAuthorFromDb(){
    $connection = connectToDb(USERNAME, PASSWORD, ADDRESS);
    $request = "SELECT id, firstname, lastname, grade FROM authors";
    $stmt = $connection ->prepare($request);
    $stmt -> execute();
    $result = $stmt->setFetchMode(PDO::FETCH_ASSOC);
    return $stmt->fetchAll();
}
function showAuthors($data, $authorid){
    foreach ($data as $x => $value ){
        if($data[$x]['id'] == $authorid){
            echo "<option selected='selected' value='" . $data[$x]['id'] . "'>" . $data[$x]['firstname'] . " " . $data[$x]['lastname'] . "</option>";
        }
        else {
            echo "<option value='" . $data[$x]['id'] . "'>" . $data[$x]['firstname'] . " " . $data[$x]['lastname'] . "</option>";
        }
    }
}
    ?>

<header>
    <a href="?cmd=show_book_list" id="book-list-link">Raamatud |</a>
    <a href="?cmd=show_book_form" id="book-form-link">Lisa raamat |</a>
    <a href="?cmd=show_author_list" id="author-list-link">Autorid |</a>
    <a href="?cmd=show_author_form" id="author-form-link">Lisa autor</a>
    <br/>
</header>
<form action="edit-delete-book.php" name="myForm">
    <table class="input-table">
        <tr>
            <td><label for="plkr" >Pealkiri: </label></td>
            <td><input type="text" id="plkr" name="title" value="<?php echo $bookData[0]['title']?>"></td>
        </tr>
        <tr>
            <td><label for="author1">Autor 1:</label></td>
            <td><select id="author1" name="author1">
                    <option value="0"></option>
                    <?php showAuthors(getAuthorFromDb(), $bookData[0]['authorid']); ?>
                </select>
            </td>
        </tr>

        <tr>
            <td><label for="author2">Autor 2:</label></td>
            <td><select id="author2">
                    <option></option>
                    <option>AA</option>
                </select></td>
        </tr>
        <tr>
            <td><label for="grade">Hinne:</label></td>
            <td>
                <input type="radio" name="grade" id="grade" value="1" <?php gradeCheck($bookData[0]['grade'], 1); ?>>1
                <label for="2"></label><input type="radio" name="grade" value="2" id="2" <?php gradeCheck($bookData[0]['grade'], 2); ?>>2
                <label for="3"></label><input type="radio" name="grade" value="3" id="3" <?php gradeCheck($bookData[0]['grade'], 3); ?>>3
                <label for="4"></label><input type="radio" name="grade" value="4" id="4" <?php gradeCheck($bookData[0]['grade'], 4); ?>>4
                <label for="5"></label><input type="radio" name="grade" value="5" id="5" <?php gradeCheck($bookData[0]['grade'], 5); ?>>5</td>
        </tr>

        <tr>
            <td><label for="isRead">Loetud</label></td>
            <td><input type="checkbox" id="isRead" name="isRead" <?php echo $read;?>><br></td>
        </tr>
        <tr>
            <input type='hidden' name='id' value='<?php echo "$id";?>'/>
            <td><input type="submit" value="Kustuta" name="deleteButton"></td>
            <td><input type="submit" value="Salvesta" name="submitButton"></td>
        </tr>

    </table>
</form>
<footer>
    ICD0007 Näidisrakendus
</footer>
</body>
</html>