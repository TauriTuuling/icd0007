<?php
const USERNAME = 'taurituuling';
const PASSWORD = 'd40c';
const ADDRESS = 'mysql:host=db.mkalmo.xyz;dbname=taurituuling';

function connectToDb($user,$pass,$address){
    return $connection = new PDO($address, $user, $pass,[PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]);
}
function getFromDb(){
    $connection = connectToDb(USERNAME, PASSWORD, ADDRESS);
    $request = "SELECT books.id, books.title, books.grade, books.authorid, authors.firstname, authors.lastname FROM books LEFT JOIN authors ON authorid=authors.id";
    $stmt = $connection ->prepare($request);
    $stmt -> execute();
    $result = $stmt->setFetchMode(PDO::FETCH_ASSOC);
    return $stmt->fetchAll();
}
function printTable($data){
    foreach ($data as $x => $value ){
        echo "<tr><td>" . "<a href='book-edit.php?id=" . $data[$x]['id'] . "'>" . $data[$x]['title'] . "</a>" . "</td>" . "<td>" . $data[$x]['firstname']. " " . $data[$x]['lastname'] . "</td>" . "<td>" . $data[$x]['grade'] . "</td>" . "</tr>";
    }
}

function validateAuthor($firstName,$lastName){
    if (strlen($firstName) > 0 and strlen($firstName) < 22 and
        strlen($lastName) > 1 and strlen($lastName) < 23){
        return true;
    }
    else{
        return false;
    }
}

function addAuthor($fname, $lname, $grade){
    $connection = connectToDb(USERNAME, PASSWORD, ADDRESS);
    $data = "INSERT INTO authors (firstname, lastname, grade) VALUES(:firstname,:lastname,:grade)";
    $stmt = $connection ->prepare($data);
    $stmt -> bindValue(":firstname", $fname);
    $stmt -> bindValue(":lastname", $lname);
    $stmt -> bindValue(":grade", $grade);
    $stmt ->execute();
    unset($stmt);

}

function validateBook($title){
    if (strlen($title) > 2 and strlen($title) < 24){
        return true;
    }
    else {return false;}
}

function addBook($title, $isread, $grade, $authorid){
    $connection = connectToDb(USERNAME, PASSWORD, ADDRESS);
    $data = "INSERT INTO books (title, isread, grade, authorid) VALUES(:title,:isread,:grade,:authorid)";
    $stmt = $connection ->prepare($data);
    $stmt -> bindValue(":title", $title);
    $stmt -> bindValue(":isread", $isread);
    $stmt -> bindValue(":grade", $grade);
    $stmt -> bindValue(":authorid", $authorid);
    $stmt ->execute();
    unset($stmt);
}

function isReadTranslate(){
    if (isset($_GET["isRead"])) {
        return 1;
    } else {
        return 0;
    }
}
function gradeTranslate(){
    if (isset($_GET['grade'])){
        return $_GET['grade'];
    }
    else {return 0;}
}

function editAuthor($id, $firstname, $lastname, $grade){
    $connection = connectToDb(USERNAME, PASSWORD, ADDRESS);
    $request = "UPDATE authors SET firstname=:firstname, lastname=:lastname, grade=:grade WHERE id=$id";
    $stmt = $connection ->prepare($request);
    $stmt -> bindValue(":firstname", $firstname);
    $stmt -> bindValue(":lastname", $lastname);
    $stmt -> bindValue(":grade", $grade);
    $stmt -> execute();
}

function deleteAuthor($id){
    $connection = connectToDb(USERNAME, PASSWORD, ADDRESS);
    $request = "DELETE FROM authors WHERE id=$id";
    $stmt = $connection ->prepare($request);
    $stmt -> execute();
}

function editBook($id, $title, $grade, $isread, $authorid){
    $connection = connectToDb(USERNAME, PASSWORD, ADDRESS);
    $request = "UPDATE books SET title=:title, isread=:isread, grade=:grade, authorid=:authorid WHERE id=$id";
    $stmt = $connection ->prepare($request);
    $stmt -> bindValue(":title", $title);
    $stmt -> bindValue(":isread", $isread);
    $stmt -> bindValue(":grade", $grade);
    $stmt -> bindValue(":authorid", $authorid);
    $stmt -> execute();
}

function deleteBook($id){
    $connection = connectToDb(USERNAME, PASSWORD, ADDRESS);
    $request = "DELETE FROM books WHERE id=$id";
    $stmt = $connection ->prepare($request);
    $stmt -> execute();
}