<?php


class authorDao
{
    private $connection;
    public function __construct(){
        $this ->connection = $this ->connectToDb();
    }
    private function connectToDb (){
        $user = 'taurituuling';
        $pass = 'd40c';
        $address = 'mysql:host=db.mkalmo.xyz;dbname=taurituuling';
        return $connection = new PDO($address, $user, $pass,[PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]);
    }
    public function getAllAuthors(){
        $this ->connectToDb();
        $request = "SELECT id, firstname, lastname, grade FROM authors";
        $stmt = $this ->connection->prepare($request);
        $stmt -> execute();
        $authors = [];
        foreach ($stmt as $row){
            $author = new author($row['id'], $row['firstname'], $row['lastname'], $row['grade']);
            $authors[] = $author;
        }
        return $authors;
    }
    public function getAuthorById($id){
        $this->connectToDb();
        $request = "SELECT id, firstname, lastname, grade FROM authors WHERE id=$id";
        $stmt = $this ->connection->prepare($request);
        $stmt -> execute();
        foreach ($stmt as $row){
            $author = new author($row['id'], $row['firstname'], $row['lastname'], $row['grade']);
        }
        return $author;
    }
}