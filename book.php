<?php


class book
{
    public $id;
    public $title;
    public $grade;
    public $isread;
    public $authorId;
    public $authorName;
    public function __construct($id, $title, $isread, $grade, $authorId, $authorName) {
        $this -> id = intval($id);
        $this -> title = $title;
        $this ->isread = $isread;
        $this -> grade = $grade;
        $this -> authorId = $authorId;
        $this -> authorName = $authorName;
    }
}