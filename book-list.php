<!DOCTYPE html>
<html lang="et">
<head>
    <meta charset="utf-8">
    <title>Harjutustund 1</title>
    <link href="styles.css" rel="stylesheet">
</head>
<body>
<?php

const USERNAME = 'taurituuling';
const PASSWORD = 'd40c';
const ADDRESS = 'mysql:host=db.mkalmo.xyz;dbname=taurituuling';

function connectToDb($user,$pass,$address){
    return $connection = new PDO($address, $user, $pass,[PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]);
}
function getFromDb(){
    $connection = connectToDb(USERNAME, PASSWORD, ADDRESS);
    $request = "SELECT books.id, books.title, books.grade, books.authorid, authors.firstname, authors.lastname FROM books LEFT JOIN authors ON authorid=authors.id";
    $stmt = $connection ->prepare($request);
    $stmt -> execute();
    $result = $stmt->setFetchMode(PDO::FETCH_ASSOC);
    return $stmt->fetchAll();
}
function printTable($data){
    foreach ($data as $x => $value ){
        echo "<tr><td>" . "<a href='book-edit.php?id=" . $data[$x]['id'] . "'>" . $data[$x]['title'] . "</a>" . "</td>" . "<td>" . $data[$x]['firstname']. " " . $data[$x]['lastname'] . "</td>" . "<td>" . $data[$x]['grade'] . "</td>" . "</tr>";
    }
}
?>
<header>
    <a href="?cmd=show_book_list" id="book-list-link">Raamatud |</a>
    <a href="?cmd=show_book_form" id="book-form-link">Lisa raamat |</a>
    <a href="?cmd=show_author_list" id="author-list-link">Autorid |</a>
    <a href="?cmd=show_author_form" id="author-form-link">Lisa autor</a>
    <br/>
</header>
<?php

if (isset($_GET["message"])) {
    $message = $_GET["message"];
    if ($message == "success") {
        echo "<p id='message-block'>success</p>";
    }
}
?>
<table class="table_head">
    <tr>
        <th class="table_col1"><strong>Pealkiri</strong></th>
        <th class="table_col2"><strong>Autorid</strong></th>
        <th><strong>Hinne</strong></th>
    </tr>
</table>


<table class="tabel1" id="myTable">
    <tr>
        <td class="table_col1"></td>
        <td class="table_col2">Autor 1</td>
        <td>Hinne 1</td>
    </tr>
    <tr>
        <td class="table_col1"><a href="book-edit.php?id=2" id="book-list-link"></a></td>
        <td class="table_col2">Autor 2</td>
        <td>Hinne 2</td>
    </tr>
    <?php printTable(getFromDb());?>
</table>
<footer>
    ICD0007 Näidisrakendus
</footer>
</body>
</html>