<?php

require_once "src/tpl.php";
require_once "book.php";
require_once "BookDao.php";
require_once "authorDao.php";
require_once "author.php";
require_once "functions.php";
$data = [ 'greeting' => 'Hello!'];

$bookDao = new BookDao();
$authorDao = new authorDao();

$cmd = 'show_book_list';
if (isset($_GET['cmd'])){
    $cmd = $_GET['cmd'];
}

if ($cmd == 'show_book_list'){

    $books = $bookDao->getallBooks();
    $message = '';
    if(isset($_GET['message']))
    {
        $message = $_GET['message'];
    }
    $data = [
        'books' =>$books
    ];
    print renderTemplate('book-list.html', $data);
}
else if ($cmd == 'add_book'){
    if(validateBook($_GET['title'])){
        addBook($_GET['title'], isReadTranslate(), gradeTranslate(), $_GET['author1']);
        $books = $bookDao->getallBooks();

        $data = [
            'books' =>$books,
            'message' =>'added'
        ];
        header("Location: ?cmd=show_book_list&message=added");
    }
    else{
        $authors = $authorDao->getAllAuthors();
        $data= [
            'title'=>$_GET['title'],
            'isRead'=>$_GET['isRead'],
            'grade'=>$_GET['grade'],
            'author1'=>$_GET['author1'],
            'message' =>'Error',
            'authors'=>$authors
        ];
        print renderTemplate('book-add.html',$data);
    }

}
else if ($cmd == 'show_book_form'){
    $authors = $authorDao->getAllAuthors();
    $data = [
        'authors' =>$authors
    ];
    print renderTemplate('book-add.html',$data);
}
else if ($cmd == 'show_author_list'){
    $authors = $authorDao->getAllAuthors();
    $message = '';
    if(isset($_GET['message']))
    {
        $message = $_GET['message'];
    }
    $data = [
        'authors' =>$authors,
        'message'=>$message
    ];
    print renderTemplate('author-list.html', $data);
}
else if ($cmd == 'show_author_form'){
    print renderTemplate('author-add.html',[]);
}
else if($cmd == 'add_author'){
    if (validateAuthor($_GET['firstName'], $_GET['lastName'])){
        addAuthor($_GET['firstName'], $_GET['lastName'],$_GET['grade']);
        header("Location: ?cmd=show_author_list&message=added");
    }
    else{
        $data = [
            'firstname'=> $_GET['firstName'],
            'lastname'=> $_GET['lastName'],
            'grade'=>$_GET['grade'],
            'message'=>'error'
        ];
        print renderTemplate('author-add.html',$data);
    }
}
else if($cmd == 'show_author_edit'){
    $data = [
        'author' => $authorDao->getAuthorById($_GET['id'])
    ];
    print renderTemplate('author-edit.html',$data);
}
else if($cmd == 'edit_author'){
    if(isset($_GET['deleteButton'])) {
        deleteAuthor($_GET['id']);
        header("Location: ?cmd=show_author_list");
    }
    else{
        editAuthor($_GET['id'], $_GET['firstName'], $_GET['lastName'], gradeTranslate());
        header("Location: ?cmd=show_author_list");
    }
}
else if($cmd == 'show_book_edit'){
    $data = [
        'book' => $bookDao->getBookById($_GET['id']),
        'authors'=>$authorDao->getAllAuthors()
    ];
    print renderTemplate('book-edit.html', $data);
}
else if ($cmd == 'edit_book'){
    if(isset($_GET['deleteButton'])){
        deleteBook($_GET['id']);
        header("Location: ?cmd=show_book_list");
    }
    else{
        editBook($_GET['id'] ,$_GET['title'],$_GET['grade'], $_GET['isRead'], $_GET['author1']);
        header("Location: ?cmd=show_book_list");
    }
}